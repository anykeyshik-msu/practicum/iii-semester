#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#define NUM_OF_PARTIES 6

typedef struct Block 
{
    uint16_t num_of_votes;
    uint16_t prev_sum;
    uint16_t *votes;
} Block;

void
correct_votes(Block *, uint16_t, uint16_t);

void
calc_sum(uint16_t *, Block *);

int
main(int argc, char **argv)
{
    //int fd = open(argv[1], O_RDONLY);
    int fd = open(argv[1], O_RDWR);

    unsigned int sum;
    uint16_t sum_of_votes[NUM_OF_PARTIES] = {0};
    Block prev_block, curr_block;

    if (argc < 2) {
        return -1;
    }

    memset(&prev_block, 0, sizeof(Block));
    memset(&curr_block, 0, sizeof(Block));
    sum = 0;

    while (read(fd, &curr_block.num_of_votes, sizeof(uint16_t)) != 0) {
        read(fd, &curr_block.prev_sum, sizeof(uint16_t));
        
        if (curr_block.prev_sum != sum) {
            correct_votes(&prev_block, curr_block.prev_sum, sum);
        }
        calc_sum(sum_of_votes, &prev_block);
        free(prev_block.votes);
        
        sum = 0;

        curr_block.votes = calloc(curr_block.num_of_votes, sizeof(uint16_t));
        for (int i = 0; i < curr_block.num_of_votes; i++) {
            read(fd, &curr_block.votes[i], sizeof(uint16_t));

            sum += curr_block.votes[i];
        }
        prev_block = curr_block;
    }

    for (int i = 0; i < NUM_OF_PARTIES; i++) {
        printf("%d\n", sum_of_votes[i]);
    }

    close(fd);
    free(curr_block.votes);

    return 0;
}

void
correct_votes(Block *block, uint16_t correct_sum, uint16_t sum)
{
    int changed;

    changed = NUM_OF_PARTIES - (correct_sum - sum);

    for (int i = 0; i < block->num_of_votes; i++) {
        if (block->votes[i] != 1) {
            block->votes[i]++;
            changed--;
        }
    }

    if (changed) {
        for (int i = 0; i < block->num_of_votes || changed == 0; i++) {
            if (block->votes[i] == 1) {
                block->votes[i]++;
                changed--;
            }
        }
    }
}

void
calc_sum(uint16_t *sum, Block *block)
{
    int part_num;
    for (int i = 0; i < block->num_of_votes; i++) {
        part_num = block->votes[i] - 1;
        sum[part_num]++;
    }
}

