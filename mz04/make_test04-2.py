#!/usr/bin/env python3

import struct
from random import randint


def gen_number():
    num = randint(20, 50)

    return num, struct.pack(">I", num)


def main():
    hinp = open('input.txt', 'w')
    inp = open('input.bin', 'wb')
    for i in range(15):
        num, pack_num = gen_number()

        hinp.write(str(num) + '\n')
        inp.write(pack_num)

    hinp.close()
    inp.close()


if __name__ == "__main__":
    main()
