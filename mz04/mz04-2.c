#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

enum 
{
    NUM_LEN = 4,
    FIRST_BIT_SHIFT = 20,
    SECOND_BIT_SHIFT = 12,
    THIRD_BIT_SHIFT = 8
};

void
convertNumber(unsigned char * restrict, unsigned long long);

int
main(int argc, char **argv)
{
    int fd;
    unsigned char buf[NUM_LEN];
    unsigned long long input;

    fd = open(argv[1], O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);

    while (scanf("%llu", &input) == 1) {
        convertNumber(buf, input);
        write(fd, buf, NUM_LEN);
    }

    close(fd);

    return 0;
}

void
convertNumber(unsigned char * restrict buf, unsigned long long bigEndian) {
    buf[0] = (bigEndian & 0xF00000) >> FIRST_BIT_SHIFT;
    buf[1] = (bigEndian & 0x0FF000) >> SECOND_BIT_SHIFT;
    buf[2] = (bigEndian & 0x000F00) >> THIRD_BIT_SHIFT;
    buf[3] =  bigEndian & 0x00000FF;
}
