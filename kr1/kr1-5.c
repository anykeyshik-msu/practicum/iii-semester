#include <stdio.h>
#include <time.h>
#include <math.h>

enum
{
    CAL_START = 1900,
    MON_IN_WEEK = 1,
    MON_IN_NEXT_WEEK = 8,
    WEEK_LEN = 7,
    FIRST_DAY = 256
};

int
moon_pahse(int, int, int);

int
main(int argc, char **argv)
{
    int year;
    struct tm timeinfo = {0};

    scanf("%d", &year);
    year -= CAL_START;

    timeinfo.tm_year = year;
    timeinfo.tm_mday = FIRST_DAY;
    mktime(&timeinfo);

    while (moon_pahse(timeinfo.tm_year, timeinfo.tm_mon, timeinfo.tm_mday) != 4) {
        timeinfo.tm_mday += 1;
        mktime(&timeinfo);
    }

    if (timeinfo.tm_wday > MON_IN_WEEK) {
        timeinfo.tm_mday += MON_IN_NEXT_WEEK - timeinfo.tm_wday;
    } else {
        timeinfo.tm_mday += MON_IN_WEEK - timeinfo.tm_wday;
    }
    mktime(&timeinfo);

    timeinfo.tm_mday += 4 * WEEK_LEN;
    mktime(&timeinfo);

    printf("%d-%d-%d", timeinfo.tm_year + CAL_START, timeinfo.tm_mon + 1, timeinfo.tm_mday);

    return 0;
}

int
moon_pahse(int year, int month, int day)
{
	/*
      calculates the moon phase (0-7), accurate to 1 segment.
      0 = > new moon.
      4 => full moon.
    */

    int phase;
    double jd, c_year, e_month;

    if (month < 3) {
        year--;
        month += 12;              
    }

    month += 1;
    c_year = 365.25 * year;
    e_month = 30.6 * month;

    jd = c_year+ e_month + day - 694039.09;    // jd is total days elapsed
    jd /= 29.53;                               
    phase = jd;                                
   	jd -= phase;		   				     
	phase = jd * 8 + 0.5;	   			    
	phase = phase & 7;		   			

    printf("%d-%d-%d\t", year + 1900, month + 1, day);
    printf("phase = %d\n", phase);

	return phase;
}
