#include <stdio.h>
#include <string.h>

void
add(int *, int, char);

int
main(int argc, char **argv)
{
    int sum = 0;

    for (int i = 1; i < argc; i++) {
        int curr_num;
        char sign;

        sscanf(argv[i], "%d%c", &curr_num, &sign);
        if (sign != '+' && sign != '-') {
            int len = strlen(argv[i]);
            sign = argv[i][len - 1];
            curr_num *= 1000;
        }

        add(&sum, curr_num, sign);
    }

    printf("%d\n", sum);

    return 0;
}

void
add(int *sum, int num, char sign)
{
    if (sign == '+') {
        *sum += num;
    } else {
        *sum -= num;
    }
}
