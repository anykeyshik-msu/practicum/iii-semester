#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <limits.h>
#include <string.h>

enum
{
    MAX_COUNT = 256
};

int
main(int argc, char **argv)
{
    if (argc < 3) {
        exit(-1);
    }

    DIR *first_dir = opendir(argv[1]);
    DIR *second_dir = opendir(argv[2]);
    if (!first_dir || !second_dir) {
        exit(-1);
    }

    struct dirent *current_dir;
    char path[MAX_COUNT][PATH_MAX];
    struct stat file_stat;
    int iter = 0;

    while ((current_dir = readdir(first_dir))) {
        if (!strcmp(current_dir->d_name, ".") || !strcmp(current_dir->d_name, "..")) {
            continue;
        }

        sprintf(path[iter], "%s", current_dir->d_name);
        if (stat(path[iter], &file_stat) != 0) {
            continue;
        } 
        if ( ((file_stat.st_mode & S_IFMT) != S_IFREG) || (access(path, W_OK)))
    }

    return 0;
}
