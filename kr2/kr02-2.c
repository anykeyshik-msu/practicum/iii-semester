#include <stdio.h>
#include <stdlib.h>

__int128_t stoi128(const char *str);
void print128(__int128_t num);

int
main(int argc, char *argv[])
{
    if (argc < 3) {
        exit(1);
    }

    __int128_t first_number = stoi128(argv[1]);
    __int128_t second_number = stoi128(argv[2]);

    first_number -= second_number;

    print128(first_number);

    return 0;
}


