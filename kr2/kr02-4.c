#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    BUF_SIZE = 4096
};

int
main(int argc, char *argv[])
{
    if (argc < 3) {
        exit(1);
    }

    int status;
    pid_t pid = fork();
    if (pid == -1) {
        exit(1);
    }

    if (!pid) {
        int in_fd = open(argv[1], O_RDONLY);
        if (in_fd == -1) {
            _exit(42);
        }

        int out_fd = open("tmp_out", O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
        if (out_fd == -1) {
            _exit(42);
        }

        if (dup2(in_fd, 0) == -1 || dup2(out_fd, 1) == -1) {
            _exit(42);
        }

        if (execlp(argv[2], argv[2], NULL) == -1) {
            _exit(42);
        }
    }

    wait(&status);
    if (WIFEXITED(status)) {
        char buf[BUF_SIZE];

        int out_fd = open(argv[1], O_WRONLY);
        if (out_fd == -1) {
            exit(1);
        }

        int in_fd = open("tmp_out", O_RDONLY);
        if (in_fd == -1) {
            exit(1);
        }

        ssize_t nread = read(in_fd, buf, sizeof(buf));
        while (nread > 0) {
            ssize_t nwrite = write(out_fd, buf, nread);

            if (nwrite < 0) {
                exit(1);
            }

            nread = read(in_fd, buf, sizeof(buf));
        }

        close(in_fd);
        close(out_fd);
    }

    unlink("tmp_out");

    return 0;
}
