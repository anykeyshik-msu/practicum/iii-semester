#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

enum
{
    BASE = 10
};

int
main(int argc, char *argv[])
{
    if (argc < 3) {
        exit(1);
    }

    int fd = open(argv[1], O_RDWR);
    if (fd == -1) {
        exit(1);
    }
    int fd_temp =
        open("temp.bin", O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR);
    if (fd_temp == -1) {
        exit(1);
    }

    long long min = strtol(argv[2], NULL, BASE);
    long long current;
    ssize_t res;
    res = read(fd, &current, sizeof(current));
    while (res == sizeof(current)) {
        if (current >= min) {
            res = write(fd_temp, &current, sizeof(current));
            if (res == -1 || res != sizeof(current)) {
                exit(1);
            }
        }

        res = read(fd, &current, sizeof(current));
    }

    if (close(fd) == -1) {
        exit(1);
    }
    if (close(fd_temp) == -1) {
        exit(1);
    }
    if (rename("temp.bin", argv[0]) != 0) {
        exit(1);
    }

    unlink("temp.bin");

    return 0;
}
