#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define KiB 1024

int
main(int argc, char const *argv[])
{
    struct stat stbuf;
    unsigned long long totalsize = 0;
    
    for (int i = 1; i < argc; i ++) {
        if (lstat(argv[i], &stbuf) != -1) {
            if ((stbuf.st_size % KiB == 0) && stbuf.st_nlink == 1) {
                if ((stbuf.st_mode & S_IFMT) == S_IFREG) {
                    totalsize += stbuf.st_size;
                }
            }
        }
    }

    printf("%lld\n", totalsize);
    
    return 0;
}
