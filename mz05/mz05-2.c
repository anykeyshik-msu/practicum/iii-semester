#include <stdio.h>
#include <stdlib.h>

const char str_perm[] = "rwxrwxrwx";

enum
{
    MASK = 1 << (sizeof(str_perm) - 2),
    PERM_LEN = sizeof(str_perm) - 1
};

int
main(int argc, char **argv)
{
    char oct_perm[PERM_LEN + 1];

    for (int i = 1; i < argc; i++) {
        unsigned short user_perm = strtol(argv[i], NULL, 8);
        unsigned short user_mask = MASK;

        for (int j = 0; j < PERM_LEN; j++) {
            if (user_mask & user_perm) {
                oct_perm[j] = str_perm[j];
            } else {
                oct_perm[j] = '-';
            }

            user_mask >>= 1;
        }
        oct_perm[PERM_LEN] = '\0';

        printf("%s\n", oct_perm);
    }

    return 0;
}
