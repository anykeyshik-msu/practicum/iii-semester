#include <stdio.h>

const char s[] = "rwxrwxrwx";

enum
{
    LEN = sizeof(s)
};

int
parse_rwx_permissions(const char *str)
{
    if (!str) {
        return -1;
    }

    int num_rules = 0;
    
    for (int i = 0; i < LEN; i++) {
        num_rules <<= 1;
        
        if (str[i] == s[i]) {
            num_rules = num_rules | 1;
        } else if (str[i] != '-') {
            return -1;
        }
    }

    return num_rules >> 1;
}
