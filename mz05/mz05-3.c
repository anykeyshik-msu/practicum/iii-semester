#include <stdio.h>
#include <inttypes.h>
#include <math.h>

int
main(int argc, char const *argv[])
{
    uint32_t n, s;
    int w;
    scanf("%" SCNu32 "%" SCNu32 "%d", &n, &s, &w);

    uint32_t nownumber = 0;
    uint32_t prevnumber = 0;

    uint32_t sigmask = 1u;
    uint32_t singbit = 1u << (n - 1);

    sigmask <<= (n - 1);
    sigmask -= 1;

    uint32_t unsmask = sigmask << 1 | 1;

    while ((nownumber >= prevnumber) && nownumber <= unsmask) {
        prevnumber = nownumber;
        nownumber += s;

        uint32_t unsign = prevnumber & unsmask;
        int32_t signnum;

        if (prevnumber & singbit) {
            signnum = -(prevnumber & sigmask);
        } else {
            signnum = prevnumber & sigmask;
        }

        printf("|%*" PRIo32 "|%*" PRIu32 "|%*" PRId32 "|\n", w, prevnumber, w, unsign, w, signnum);
    }
    
    return 0;
}
