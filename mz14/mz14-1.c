#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    BASE = 10
};

int
main(int argc, char *argv[])
{
    if (argc < 4) {
        exit(1);
    }

    int nproc = strtol(argv[1], NULL, BASE);
    key_t key = strtol(argv[2], NULL, BASE);
    int maxval = strtol(argv[3], NULL, BASE);

    int sem_id = semget(key, nproc, 0666 | IPC_CREAT | IPC_EXCL);
    if (sem_id < 0) {
        exit(1);
    }

    int shm_id = shmget(key, 2 * sizeof(maxval), 0666 | IPC_CREAT | IPC_EXCL);
    if (shm_id < 0) {
        exit(1);
    }

    int *shm = shmat(shm_id, NULL, 0);
    shm[0] = 0;
    shm[1] = 0;

    for (int i = 0; i < nproc; i++) {
        pid_t pid = fork();
        if (pid == -1) {
            exit(1);
        }

        if (!pid) {
            while (semop(sem_id, (struct sembuf[]){ { i, -1, 0 } }, 1) != -1) {
                printf("%d %d %d\n", i + 1, shm[0], shm[1]);
                fflush(stdout);

                if (shm[0] >= maxval) {
                    semctl(sem_id, 0, IPC_RMID);
                    exit(0);
                }

                shm[0]++;
                shm[1] = i + 1;
                int tmp = 1;
                for (int j = 0; j < 4; j++) {
                    tmp = tmp * shm[0] % nproc;
                }
                tmp %= nproc;

                semop(sem_id, (struct sembuf[]){ { tmp, 1, 0 } }, 1);
            }

            exit(0);
        }
    }

    semop(sem_id, (struct sembuf[]){ { 0, 1, 0 } }, 1);

    while (wait(NULL) > 0) {
    }
    shmdt(shm);
    semctl(sem_id, 0, IPC_RMID);
    shmctl(shm_id, 0, IPC_RMID);

    return 0;
}
