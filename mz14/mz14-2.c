#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    BASE = 10
};

void
operation(int *data, int ind1, int ind2, int value);

int
main(int argc, char *argv[])
{
    if (argc < 5) {
        exit(1);
    }

    int count = strtol(argv[1], NULL, BASE);
    key_t key = strtol(argv[2], NULL, BASE);
    int nproc = strtol(argv[3], NULL, BASE);
    int itercount = strtol(argv[4], NULL, BASE);

    int sem_id = semget(key, count, 0666 | IPC_CREAT | IPC_EXCL);
    if (sem_id < 0) {
        exit(1);
    }

    int shm_id =
      shmget(key, count * sizeof(itercount), 0666 | IPC_CREAT | IPC_EXCL);
    if (shm_id < 0) {
        exit(1);
    }
    int *shm = shmat(shm_id, NULL, 0);

    for (int i = 0; i < count; i++) {
        scanf("%d", &shm[i]);
        semctl(sem_id, i, SETVAL, 1);
    }

    for (int i = 0; i < nproc; i++) {
        pid_t pid = fork();
        if (pid == -1) {
            exit(1);
        }

        if (!pid) {
            int seed = strtoul(argv[5 + i], NULL, BASE);
            srand(seed);

            for (int j = 0; j < itercount; j++) {
                int ind1 = rand() % count;
                int ind2 = rand() % count;
                int value = rand() % 10;

                if (ind1 != ind2) {
                    struct sembuf sops_down[2] = { { ind1, -1, SEM_UNDO },
                                                   { ind2, -1, SEM_UNDO } };
                    struct sembuf sops_up[2] = { { ind1, 1, SEM_UNDO },
                                                 { ind2, 1, SEM_UNDO } };
                    semop(sem_id, sops_down, 2);
                    operation(shm, ind1, ind2, value);
                    semop(sem_id, sops_up, 2);
                }
            }

            shmdt(shm);
            _exit(0);
        }
    }

    while (wait(NULL) > 0) {
    }

    for (int i = 0; i < count; i++) {
        printf("%d ", shm[i]);
    }
    fputs("\n", stdout);
    fflush(stdout);

    shmdt(shm);
    shmctl(shm_id, IPC_RMID, NULL);
    semctl(sem_id, count, IPC_RMID, 0);

    return 0;
}
