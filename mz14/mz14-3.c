#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <unistd.h>

typedef struct Message
{
    long type;
    long long value;
} Message;

enum
{
    BASE = 10,
    ACCESS = 0600
};

void
clear_exit(pid_t *pids, size_t size);

int
main(int argc, char *argv[])
{
    if (argc < 6) {
        exit(1);
    }

    key_t key = strtol(argv[1], NULL, BASE);
    int nproc = strtol(argv[2], NULL, BASE);
    long long value1 = strtoll(argv[3], NULL, BASE);
    long long value2 = strtoll(argv[4], NULL, BASE);
    long long maxval = strtoll(argv[5], NULL, BASE);

    int msgid = msgget(key, ACCESS | IPC_CREAT);
    pid_t *pids = calloc(nproc, sizeof(*pids));

    for (int i = 0; i < nproc; i++) {
        pids[i] = fork();

        if (pids[i] == -1) {
            msgctl(msgid, IPC_RMID, NULL);
            clear_exit(pids, i);
        }

        if (!pids[i]) {
            while (1) {
                Message msg1;
                Message msg2;

                if (msgrcv(msgid,
                           &msg1,
                           sizeof(msg1) - sizeof(msg1.type),
                           i + 1,
                           0) < 0) {
                    break;
                }
                if (msgrcv(msgid,
                           &msg2,
                           sizeof(msg2) - sizeof(msg2.type),
                           i + 1,
                           0) < 0) {
                    break;
                }

                long long curr_val = msg1.value + msg2.value;
                msg1.type = curr_val % nproc + 1;
                msg2.type = curr_val % nproc + 1;
                msg1.value = msg2.value;
                msg2.value = curr_val;

                printf("%d %lld\n", i, curr_val);
                fflush(stdout);

                if (curr_val > maxval) {
                    msgctl(msgid, IPC_RMID, NULL);
                    break;
                }

                msgsnd(msgid, &msg1, sizeof(msg1) - sizeof(msg1.type), 0);
                msgsnd(msgid, &msg2, sizeof(msg2) - sizeof(msg2.type), 0);
            }

            _exit(0);
        }
    }

    msgsnd(msgid,
           &(Message){ .type = 1, .value = value1 },
           sizeof(struct Message) - sizeof(long),
           0);
    msgsnd(msgid,
           &(Message){ .type = 1, .value = value2 },
           sizeof(struct Message) - sizeof(long),
           0);

    while (wait(NULL) > 0) {
    }

    return 0;
}

void
clear_exit(pid_t *pids, size_t size)
{
    for (int i = 0; i < size; i++) {
        fflush(stdout);
        kill(pids[i], SIGKILL);
    }

    while (wait(NULL) > 0) {
    }

    exit(1);
}
