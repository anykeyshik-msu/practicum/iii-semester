#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    BASE = 10,
    ACCESS = 0600
};

int
main(int argc, char *argv[])
{
    int nproc = strtol(argv[1], NULL, BASE);

    int sem_id = semget(IPC_PRIVATE, nproc, ACCESS | IPC_CREAT | IPC_EXCL);
    if (sem_id == -1) {
        exit(1);
    }

    setbuf(stdin, NULL);
    for (int i = 0; i < nproc; i++) {
        pid_t pid = fork();

        if (pid == -1) {
            exit(1);
        }

        if (!pid) {
            struct sembuf sem_down[] = {
                { .sem_num = i, .sem_op = -1, .sem_flg = 0 }
            };

            while (semop(sem_id, sem_down, 1) != -1) {
                int value;

                if (scanf("%d", &value) != 1) {
                    semctl(sem_id, 0, IPC_RMID);
                    exit(0);
                }

                printf("%d %d\n", i, value);
                fflush(stdout);

                int next = value % nproc;
                if (value < 0) {
                    next = (next + nproc) % nproc;
                }

                struct sembuf sem_up[] = {
                    { .sem_num = next, .sem_op = 1, .sem_flg = 0 }
                };
                semop(sem_id, sem_up, 1);
            }

            _exit(0);
        }
    }

    struct sembuf sem_end[] = { { 0, 1, 0 } };
    semop(sem_id, sem_end, 1);

    while (wait(NULL) > 0) {
    }
    semctl(sem_id, 0, IPC_RMID);

    return 0;
}
