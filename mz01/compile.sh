#!/bin/bash

echo "Enter task name:"
read name

echo "Compile..."
gcc -m32 -O2 -std=gnu18 -Wall -Werror -Wno-pointer-sign -Werror=vla -lm $name.c -o build/$name
echo "Done!"

./build/$name
