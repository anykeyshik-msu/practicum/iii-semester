#!/bin/bash

echo "Enter task name:"
read name

echo "Compile..."
gcc -m32 -Og -g -std=gnu18 -Wall -Werror -Wno-pointer-sign -Werror=vla -lm $name.c -o build/$name
echo "Done!"

./build/$name
