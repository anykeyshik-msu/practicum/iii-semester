#include <stdio.h>
#include <stdlib.h>

void 
swap(int *first, int *second);

int 
next_set(int *set, int size);

void 
print_set(int *set, int size);

int
main(void)
{
    int size;
    int *set;

    scanf("%d", &size);
    set = (int *)calloc(size, sizeof(int));
    for (int i = 0; i < size; i++) {
        set[i] = i + 1;
    }

    print_set(set, size);
    while (next_set(set, size)) {
        print_set(set, size);
    }

    free(set);

    return 0;
}

void
swap(int *first, int *second) 
{
    *first ^= *second;
    *second ^= *first;
    *first ^= *second;
}

int
next_set(int *set, int size)
{

}
