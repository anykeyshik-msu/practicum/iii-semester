#include <stdio.h>

const int ROAD_LEN = 109;

int
main(void)
{
    int speed, time, route_len;

    scanf("%d%d", &speed, &time);
    
    route_len = speed * time;
    route_len %= ROAD_LEN;
    route_len = (route_len < 0 ? ROAD_LEN + route_len : route_len);

    printf("%d", route_len);

    return 0;
}

