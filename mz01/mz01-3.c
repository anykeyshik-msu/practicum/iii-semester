#include <stdio.h>
#include <stdlib.h>

void reverse(int *, int *);

void swap(int *, int *);

int
main(void)
{
    int array_len;
    int *array;
    int left, right, shift;

    scanf("%d", &array_len);
    array = (int *) calloc(array_len, sizeof(int));
    for (int i = 0; i < array_len; i++) {
        scanf("%d", &array[i]);
    }
    scanf("%d%d%d", &left, &right, &shift);

    left -= 1;
    right -= 1;
    shift %= (right - left);
    reverse(&array[left], &array[shift]);
    for (int i = 0; i < array_len; i++) {
        printf("%d ", array[i]);
    }
    printf("\n\n");
    reverse(&array[shift + 1], &array[right]);
    for (int i = 0; i < array_len; i++) {
        printf("%d ", array[i]);
    }
    printf("\n\n");
    reverse(&array[left], &array[right]);

    for (int i = 0; i < array_len; i++) {
        printf("%d ", array[i]);
    }

    free(array);

    return 0;
}

void
reverse(int *left_border, int *right_border)
{
    fprintf(stderr, "START\n=============================\nleft_border = %d\tright_border = %d\n",
            *left_border, *right_border);
    int *j = right_border;
    for (int *i = left_border; i < j; i++) {
        fprintf(stderr, "i = %d\tj = %d\n", *i, *j);
        swap(i, j);
        j--;
    }
}

void
swap(int *x, int *y)
{
    *x ^= *y;
    *y ^= *x;
    *x ^= *y;
}

