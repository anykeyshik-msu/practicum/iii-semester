#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

int target_pid;
int fd;
unsigned char byte;
volatile sig_atomic_t bit_number = 0;

void
signal_reader(int signal);
void
signal_writer(int signal);
void
signal_pid(int signal, siginfo_t *signal_info, void *unused);

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        exit(1);
    }

    struct sigaction first_action = { .sa_flags = SA_SIGINFO | SA_RESETHAND,
                                      .sa_sigaction = signal_pid };
    sigaction(SIGINT, &first_action, NULL);

    signal(SIGUSR1, signal_writer);
    signal(SIGUSR2, signal_writer);
    signal(SIGIO, signal_writer);

    int first_pid = fork();
    if (first_pid == -1) {
        exit(1);
    }
    if (!first_pid) {
        while (1) {
            pause();
        }
    }

    struct sigaction second_action = { .sa_handler = SIG_DFL, .sa_flags = 0 };
    sigaction(SIGINT, &second_action, NULL);

    signal(SIGALRM, signal_reader);

    int second_pid = fork();
    if (second_pid == -1) {
        exit(1);
    }
    if (!second_pid) {
        target_pid = first_pid;

        kill(target_pid, SIGINT);

        fd = open(argv[1], O_RDONLY);
        if (read(fd, &byte, sizeof(byte)) < 1) {
            close(fd);
            kill(first_pid, SIGIO);
            _exit(0);
        }
        kill(getpid(), SIGALRM);

        while (1) {
            pause();
        }
    }

    wait(NULL);
    wait(NULL);

    return 0;
}

void
signal_reader(int signal)
{
    if (bit_number >= CHAR_BIT) {
        bit_number = 0;

        if (read(fd, &byte, sizeof(byte)) < 1) {
            close(fd);
            kill(target_pid, SIGIO);
            _exit(0);
        }
    }

    unsigned char mask = 1u << bit_number;
    unsigned char bit = (byte & mask) >> bit_number;
    if (bit) {
        kill(target_pid, SIGUSR2);
    } else {
        kill(target_pid, SIGUSR1);
    }

    bit_number++;
}

void
signal_writer(int signal)
{
    if (bit_number == CHAR_BIT) {
        write(1, &byte, sizeof(byte));

        byte = 0;
        bit_number = 0;
    }
    bit_number++;

    if (signal == SIGUSR2) {
        unsigned char mask = 1u << (bit_number - 1);
        byte |= mask;
    } else if (signal == SIGIO) {
        _exit(0);
    }

    kill(target_pid, SIGALRM);
}

void
signal_pid(int signal, siginfo_t *signal_info, void *unused)
{
    target_pid = signal_info->si_pid;
}
