#include <dlfcn.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    BASE = 10
};

double
square(int left, int right, int n, double (*func)(double));

int
main(int argc, char *argv[])
{
    if (argc < 5) {
        exit(1);
    }

    int left = strtol(argv[1], NULL, BASE);
    int right = strtol(argv[2], NULL, BASE);
    int n = strtol(argv[3], NULL, BASE);
    char *func = argv[4];

    char src_name[PATH_MAX];
    char name[PATH_MAX];
    char cwd[PATH_MAX];
    unsigned long long salt;

    FILE *random = fopen("/dev/urandom", "r");
    if (!random) {
        exit(1);
    }
    fscanf(random, "%llu", &salt);
    fclose(random);

    if (!getcwd(cwd, sizeof(cwd))) {
        exit(1);
    }

    if (snprintf(
          src_name, sizeof(src_name), "%s/%llu_%u.c", cwd, salt, getpid()) >=
        sizeof(src_name) - 1) {
        exit(1);
    }
    if (snprintf(name, sizeof(name), "%s/%llu_%u.so", cwd, salt, getpid()) >=
        sizeof(name) - 1) {
        exit(1);
    }

    FILE *src = fopen(src_name, "w");
    if (!src) {
        exit(1);
    }
    fputs("#include<math.h>\n", src);
    fputs("double func(double x)\n{\n", src);
    fprintf(src, "return %s;\n", func);
    fputs("}\n", src);
    fclose(src);

    pid_t pid = fork();
    if (pid == -1) {
        exit(1);
    }
    if (!pid) {
        execlp("gcc", "gcc", "-shared", "-fPIC", "-lm",
                "-o", name, src_name, NULL);
        _exit(1);
    }
    wait(NULL);

    void *handle = dlopen(name, RTLD_LAZY);
    if (!handle) {
        remove(src_name);
        remove(name);

        exit(1);
    }
    double (*dlfunc)(double) = dlsym(handle, "func");
    if (!dlfunc) {
        remove(src_name);
        remove(name);
        dlclose(handle);

        exit(1);
    }

    double answer = square(left, right, n, dlfunc);
    printf("%.10g\n", answer);

    remove(src_name);
    remove(name);
    dlclose(handle);

    return 0;
}

double
square(int left, int right, int n, double (*func)(double))
{
    double sum = 0;
    double dx = (double) (right - left) / n;
    double x = left;

    for (int i = 0; i < n; i++) {
        sum += func(x) * dx;
        x += dx;
    }

    return sum;
}
