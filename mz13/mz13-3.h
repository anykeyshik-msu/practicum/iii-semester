#include <stdlib.h>

#include <stdio.h>

enum
{
    INIT_SIZE = 10
};

double ***
transpose(double ***arr)
{
    size_t size = INIT_SIZE;

    double ***new_arr = (double ***) calloc(size + 1, sizeof(double **));
    for (int i = 0; i < size; i++) {
        new_arr[i] = (double **) calloc(size + 1, sizeof(double *));
        for (int j = 0; j < size; j++) {
            new_arr[i][j] = (double *) calloc(1, sizeof(double));
        }
    }

    int i = 0;
    int j = 0;
    int max = 0;

    while (arr[j][i] != NULL) {
        while (arr[j] != NULL) {
            *new_arr[i][j] = *arr[j][i];

            j++;
        }

        i++;
        if (j > max) {
            max = j;
        }
        j = 0;
    }

    return new_arr;
}
