#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

enum
{
    BASE = 10,
    WRITE = 'W',
    READ = 'R'
};

int
main(int argc, char *argv[])
{
    if (argc < 4) {
        exit(1);
    }

    int cache_size = strtol(argv[2], NULL, BASE);
    int block_size = strtol(argv[3], NULL, BASE);

    int block_count = cache_size / block_size;
    int miss_counter = 0;

    int *cache = malloc(block_count * sizeof(*cache));
    for (int i = 0; i < block_count; i++) {
        cache[i] = -1;
    }

    char io_type;
    char op_type;
    uint32_t addr;
    while (scanf("%c%c %x %*d %*d%*c", &io_type, &op_type, &addr) == 3) {
        int block_number = addr / block_size;
        int cache_addr = block_number % block_count;

        if (io_type == WRITE) {
            cache[cache_addr] = block_number;
        }

        if (io_type == READ) {
            if (cache[cache_addr] != block_number && cache[cache_addr] != -1) {
                miss_counter++;
            }

            cache[cache_addr] = block_number;
        }
    }

    printf("%d\n", miss_counter);

    free(cache);

    return 0;
}
