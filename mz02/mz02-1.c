#include <stdio.h>
#include <stdlib.h>

char *
inputString(void);

int
main(void)
{
    char *host, *login, *password;

    host = inputString();
    login = inputString();
    password = inputString();

    printf("[Host:%s,Login:%s,Password:%s]\n", host, login, password);

    free(host);
    free(login);
    free(password);

    return 0;
}

char *
inputString(void)
{
    char *str;
    int ch;
    size_t len;
    size_t size;

    len = 0;
    size = 20;

    str = realloc(NULL, sizeof(char) * size);
    if (!str) {
                return str;
    }

    while (EOF != (ch = fgetc(stdin)) && ch != '\n') {
        str[len++] = ch;

        if (len == size) {
            str = realloc(str, sizeof(char) * (size += 16));
            if (!str) {
                return str;
            }
        }
    }
    str[len++] = '\0';

    return realloc(str, sizeof(char)*len);
}

