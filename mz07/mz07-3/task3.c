#include <stdio.h>
#include <stdlib.h>
#include "cJSON.h"

void task3()
{
	cJSON * item;

	item = cJSON_Parse("[1,2,3]");

    char *text = cJSON_Print(item);
    puts(text);
    free(text);

	cJSON_ReplaceItemInArray(item, 1, cJSON_CreateFalse());

    text = cJSON_Print(item);
    puts(text);
    free(text);
}

int main(int argc, char ** argv)
{
	task3();

	return 0;
}

