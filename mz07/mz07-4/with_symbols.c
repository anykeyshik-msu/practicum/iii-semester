#include <stdio.h>

void
f(void)
{
    int k;
    long long int x;

    x = 178LL;
    for (k = 0; k <= 99999; k++) {
        x = (42 * x + 82732) % 1000000007;

        if (k == 9999) {
            printf("Iter = %d\tx = %lld", k + 1, x);
        }
    }
}

int
main(void)
{
    f();

    return 0;
}
