#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    int parent_num;
    int num;
    int parent_pid = getpid();
    int pid, status;

    if (scanf("%d", &parent_num) != 1) {
        exit(0);
    }

    while (1) {

        pid = fork();
        if (pid == -1) {
            exit(1);
        }

        if (!pid) {
            if (scanf("%d", &num) != 1) {
                exit(0);
            }
        }

        if (pid) {
            wait(&status);
            if (status) {
                if (getpid() == parent_pid) {
                    printf("-1\n");
                    exit(0);
                } else {
                    exit(1);
                }
            }
            break;
        }
    }
    if (getpid() != parent_pid) {
        printf("%d\n", num);
    } else {
        printf("%d\n", parent_num);
    }

    return 0;
}
