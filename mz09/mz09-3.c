#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    BASE = 10,
    BUF_SIZE = 8
};

int
main()
{
    for (int i = 1; i <= 3; i++) {
        int pid = fork();

        if (!pid) {
            char buf[BUF_SIZE];
            int num;

            int read_cnt = read(0, buf, sizeof(buf));
            if (read_cnt == -1) {
                exit(1);
            } else {
                buf[read_cnt - 1] = '\0';
            }
            num = strtol(buf, NULL, BASE);

            printf("%d %d\n", i, num * num);
            fflush(stdout);

            exit(0);
        }
    }

    while (wait(NULL) != -1) {
    }

    return 0;
}
