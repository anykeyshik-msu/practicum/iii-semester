#include <unistd.h>
#include <stdio.h>
#include <wait.h>

int proc(void)
{
    char tmp;
    int pid = fork();
    printf("\t===> Child with pid = %u\n", pid);
    if (!pid) {
        write(1, "1\n", 2);
    }

    scanf("%c", &tmp);

    return pid;
}

int
main(void)
{
    int status;

    printf("Current process id = %u\n", getpid());

    proc(), proc(), proc();

    printf("End of process with id = %u\n", getpid());

    while(wait(&status) != -1);

    return 0;
}

