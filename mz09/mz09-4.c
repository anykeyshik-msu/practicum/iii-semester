#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    int pid;
    int N;

    scanf("%d", &N);

    printf("%d", 1);
    fflush(stdout);

    for (int i = 2; i < N + 1; i++) {
        printf(" %d", i);
        fflush(stdout);

        pid = fork();
        if (pid == -1) {
            exit(1);
        }

        if (pid) {
            wait(NULL);
            exit(0);
        }
    }

    printf("\n");

    return 0;
}
