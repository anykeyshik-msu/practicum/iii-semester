#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int
main(void)
{
    int pid, status;

    if ( (pid = fork()) == 0 ) {
        if ( (pid = fork() == 0) ) {
            fputs("3 ", stdout);

            return 0;
        } else {
            waitpid(pid, &status, 0);

            fputs("2 ", stdout);

            return 0;
        }
    } else {
        waitpid(pid, &status, 0);

        puts("1");
    }

    return 0;
}

