#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    if (argc < 5) {
        exit(1);
    }

    int pid, status;

    pid = fork();
    if (pid == -1) {
        exit(1);
    }

    if (!pid) {
        int in_fd = open(argv[2], O_RDONLY);
        if (in_fd == -1) {
            _exit(42);
        }
        int out_fd = open(argv[3], O_WRONLY | O_APPEND | O_CREAT, 0660);
        if (out_fd == -1) {
            _exit(42);
        }
        int err_fd = open(argv[4], O_WRONLY | O_CREAT, 0660);
        if (err_fd == -1) {
            _exit(42);
        }

        if (dup2(in_fd, 0) == -1 || dup2(out_fd, 1) == -1 ||
            dup2(err_fd, 2) == -1) {
            _exit(42);
        }

        if (execlp(argv[1], argv[1], NULL) == -1) {
            _exit(42);
        }
    }

    wait(&status);
    printf("%d\n", status);

    return 0;
}
