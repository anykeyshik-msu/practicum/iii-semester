#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int
test(const char *path)
{
    int pid;

    pid = fork();
    if (pid == -1) {
        return 0;
    }

    if (!pid) {
        execlp(path, path, NULL);

        _exit(1);
    } else {
        int status;
        wait(&status);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            return 1;
        }
    }

    return 0;
}

int
main(int argc, char *argv[])
{
    if (argc < 4) {
        exit(1);
    }

    return !((test(argv[1]) || test(argv[2])) && test(argv[3]));
}
