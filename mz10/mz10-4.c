#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        exit(1);
    }

    char name[PATH_MAX];
    const char *path;

    path = getenv("XDG_RUNTIME_DIR");
    if (path == NULL) {
        path = getenv("TMPDIR");
        if (path == NULL) {
            path = "/tmp";
        }
    }

    unsigned long long salt;

    FILE *random = fopen("/dev/urandom", "r");
    if (!random) {
        exit(1);
    }
    fscanf(random, "%llu", &salt);
    fclose(random);

    if (snprintf(name, sizeof(name), "%s/%llu_%u.py", path, salt, getpid()) >=
        sizeof(name) - 1) {
        exit(1);
    }

    FILE *script = fopen(name, "w");
    if (!script) {
        exit(1);
    }
    if (chmod(name, S_IRWXU) == -1) {
        exit(1);
    }

    fputs("#!/usr/bin/env python3\n", script);
    fputs("from os import remove\n", script);
    fputs("from sys import argv\n", script);
    fprintf(script, "print(%s", argv[1]);
    for (int i = 2; i < argc; i++) {
        fprintf(script, " * %s", argv[i]);
    }
    fputs(")\n", script);
    fputs("remove(argv[0])", script);
    fflush(script);

    fclose(script);

    execl(name, name, NULL);

    return 0;
}
