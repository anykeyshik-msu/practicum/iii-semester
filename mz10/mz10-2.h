#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int mysys(const char *str)
{
    int status;
    int pid = fork();

    if (pid == -1) {
        return -1;
    }
    
    if (!pid) {
        execlp("/bin/sh", "sh", "-c", str, NULL);
        _exit(127);
    }
    
    waitpid(pid, &status, 0);
    if (WIFSIGNALED(status)) {
        return 128 + WTERMSIG(status);            
    } else {
        return WEXITSTATUS(status);            
    }
    
    return -1;
}
