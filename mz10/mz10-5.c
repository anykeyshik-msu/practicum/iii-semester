#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        exit(1);
    }

    char src_name[PATH_MAX];
    char name[PATH_MAX];
    unsigned long long salt;

    FILE *random = fopen("/dev/urandom", "r");
    if (!random) {
        exit(1);
    }
    fscanf(random, "%llu", &salt);
    fclose(random);

    if (snprintf(src_name, sizeof(src_name), "%llu_%u.c", salt, getpid()) >=
        sizeof(src_name) - 1) {
        exit(1);
    }
    if (snprintf(name, sizeof(name), "./%llu_%u", salt, getpid()) >=
        sizeof(name) - 1) {
        exit(1);
    }

    FILE *src = fopen(src_name, "w");
    if (!src) {
        exit(1);
    }

    fputs("#include <stdio.h>\n", src);
    fputs("int main(int argc, char *argv[]) {\n", src);
    fputs("int x;\n", src);
    fputs("char *reject = \"reject\", *summon = \"summon\", *disqualify = "
            "\"disqualify\";\n",
            src);
    fputs("while (scanf(\"\%d\", &x) == 1) {\n", src);
    fputs("printf(\"\%s\\n\", ", src);
    fprintf(src, "%s);}\n", argv[1]);
    fputs("remove(argv[1]);\n", src);
    fputs("return 0;}", src);

    fclose(src);

    if (!fork()) {
        execlp("gcc", "gcc", "-o", name, src_name, NULL);

        exit(1);
    } else {
        int status;
        wait(&status);

        if (!status) {
            remove(src_name);

            execlp(name, name, name, NULL);

            puts("Error!");
            _exit(1);
        }

        exit(status);
    }

    return 0;
}
