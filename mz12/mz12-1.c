#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

enum
{
    MAX_COUNT = 4
};

volatile sig_atomic_t counter = 0;

void
handler(int signal);

int
main(void)
{
    struct sigaction action = { .sa_handler = handler, .sa_flags = 0 };
    sigaction(SIGHUP, &action, NULL);

    printf("%d\n", getpid());
    fflush(stdout);

    while (1) {
        pause();
    }

    return 0;
}

void
handler(int signal)
{
    if (counter > MAX_COUNT) {
        exit(0);
    } else {
        printf("%d\n", counter);
        fflush(stdout);

        counter++;
    }
}
