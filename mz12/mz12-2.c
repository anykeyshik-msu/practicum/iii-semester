#include <errno.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

enum
{
    ADD = 1,
    MULTIPLY = 2
};

volatile sig_atomic_t state = 1;

void
handler(int signal);

int
main(void)
{
    struct sigaction action = { .sa_handler = handler, .sa_flags = SA_RESTART };

    sigaction(SIGINT, &action, NULL);
    sigaction(SIGQUIT, &action, NULL);

    printf("%d\n", getpid());
    fflush(stdout);

    int32_t sum = 0;
    while (1) {
        intmax_t input;
        if (scanf("%jd", &input) != 1) {
            if (errno == EINTR) {
                continue;
            }
            exit(0);
        }

        if (state == ADD) {
            sum += input;
        } else {
            sum *= input;
        }

        printf("%jd\n", (intmax_t) sum);
        fflush(stdout);
    }

    return 0;
}

void
handler(int signal)
{
    if (signal == SIGINT) {
        state = ADD;
    } else if (signal == SIGQUIT) {
        state = MULTIPLY;
    }
}
