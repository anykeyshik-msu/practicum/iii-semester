#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    BASE = 10,
    FIRST = 1,
    SECOND = 2
};

volatile sig_atomic_t flag = 0;

void
handler(int signal);

void
proc(FILE *input, FILE *output, int curr, int max_value);

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        exit(1);
    }

    struct sigaction action = { .sa_handler = handler, .sa_flags = 0 };
    sigaction(SIGUSR1, &action, NULL);

    int max_value = strtol(argv[1], NULL, BASE);

    int pid1, pid2;
    int fd[2];

    pipe(fd);
    FILE *input = fdopen(fd[0], "r");
    FILE *output = fdopen(fd[1], "w");

    pid1 = fork();
    if (!pid1) {
        proc(input, output, FIRST, max_value);
        fclose(input);
        fclose(output);
        close(fd[0]);
        close(fd[1]);

        exit(0);
    }

    pid2 = fork();
    if (!pid2) {
        proc(input, output, SECOND, max_value);
        fclose(input);
        fclose(output);
        close(fd[0]);
        close(fd[1]);

        exit(0);
    }

    fprintf(output, "%d\n", 1);
    fflush(output);
    fprintf(output, "%d\n", pid2);
    fflush(output);

    fclose(input);
    fclose(output);

    kill(pid1, SIGUSR1);

    int finish = wait(NULL);
    if (finish == pid1) {
        kill(pid2, SIGKILL);
    } else {
        kill(pid1, SIGKILL);
    }

    wait(NULL);

    puts("Done");
    fflush(stdout);

    return 0;
}

void
handler(int signal)
{
    flag = 1;
}

void
proc(FILE *input, FILE *output, int proc_number, int max_value)
{
    while (1) {
        int target_pid;
        int curr_value;

        fflush(stdout);

        while (!flag) {
            pause();
        }

        flag = 0;
        fscanf(input, "%d%d", &curr_value, &target_pid);
        if (curr_value >= max_value) {
            return;
        }
        printf("%d %d\n", proc_number, curr_value);
        fflush(stdout);
        curr_value++;

        fprintf(output, "%d\n", curr_value);
        fflush(output);
        fprintf(output, "%d\n", getpid());
        fflush(output);

        kill(target_pid, SIGUSR1);
    }
}
