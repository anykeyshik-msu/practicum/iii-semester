#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

enum
{
    MAX_COUNT = 2,
    FIRST_PRIME = 2
};

volatile sig_atomic_t count = 0;
volatile sig_atomic_t current = 0;

void
handler(int signal);

int
prime(int num);

int
main(void)
{
    struct sigaction action = { .sa_handler = handler, .sa_flags = SA_RESTART };
    sigaction(SIGINT, &action, NULL);

    int low;
    int high;

    scanf("%d%d", &low, &high);
    printf("%d\n", getpid());
    fflush(stdout);

    for (int i = low; i < high; i++) {
        if (prime(i)) {
            current = i;
        }
    }

    puts("-1");
    fflush(stdout);
}

void
handler(int signal)
{
    if (signal == SIGTERM) {
        exit(0);
    }
    if (count > MAX_COUNT) {
        exit(0);
    } else {
        printf("%d\n", current);
        fflush(stdout);
        count++;
    }
}

int
prime(int num)
{
    int max = (int) sqrt(num);

    for (int i = FIRST_PRIME; i <= max; i++) {
        if (num % i == 0) {
            return 0;
        }
    }

    return 1;
}
