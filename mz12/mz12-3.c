#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    BASE = 10
};

void
exec(int start, int end, char *argv[], int *count);

int
main(int argc, char *argv[])
{
    int count = 0;
    int proc_num = strtol(argv[1], NULL, BASE);
    int limit = argc > 2 + proc_num ? 2 + proc_num : argc;

    exec(2, limit, argv, &count);
    exec(limit, argc, argv, &count);

    printf("%d\n", count);
    fflush(stdout);

    return 0;
}

void
exec(int start, int end, char *argv[], int *count)
{
    for (int i = start; i < end; i++) {
        char path[PATH_MAX];
        FILE *file = fopen(argv[i], "r");
        fgets(path, PATH_MAX, file);
        path[strlen(path) - 1] = '\0';

        int pid = fork();
        if (!pid) {
            execl(path, path, NULL);
            _exit(1);
        } else {
            int status;
            wait(&status);

            if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                (*count)++;
            }
        }
    }
}
