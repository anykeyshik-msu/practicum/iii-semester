#include <stdio.h>

#include "mz03-4.h"

int
main(void)
{
    int a, b;

    scanf("%d%d", &a, &b);
    printf("%d\n", satsum(a, b));

    return 0;
}

