#include <limits.h>
#include <stddef.h>

enum 
{
    NUM_OF_BITS = sizeof(STYPE) * CHAR_BIT
};

STYPE
bit_reverse(STYPE value)
{
    UTYPE ONE = 1;
    UTYPE reverse = 0;

    for (size_t i = 0; i < NUM_OF_BITS; i++) {
        if ( (value & (ONE << i)) ) {
            reverse |= ONE << ((NUM_OF_BITS - 1) - i);  
        }
    }

    return (STYPE) reverse;
}

