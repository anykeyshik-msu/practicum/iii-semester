#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char **argv)
{
    long long positive_sum = 0;
    long long negative_sum = 0;

    for (int i = 1; i < argc; i++) {
        long long cur_num = strtoll(argv[i], NULL, 10);

        if (cur_num < 0) {
            negative_sum += cur_num;
        } else if (cur_num > 0) {
            positive_sum += cur_num;
        }
    }

    printf("%lld\n%lld\n", positive_sum, negative_sum);

    return 0;
}
