#include <stdio.h>
#include <math.h>
#include <stdlib.h>

enum
{
    PERCENTS = 100,
    ROUND_PERCISION = 10000
};

int
main(int argc, char **argv)
{
    double money, daily_delta;

    if (argc < 2) {
        return 0;
    }

    money = strtod(argv[1], NULL);
    for (int i = 2; i < argc; i++) {
        daily_delta = strtod(argv[i], NULL);

        daily_delta = (daily_delta / PERCENTS) + 1.0;
        
        money *= daily_delta;
        money = round(money * ROUND_PERCISION) / ROUND_PERCISION;
    }

    printf("%.4lf\n", money);

    return 0;
}

