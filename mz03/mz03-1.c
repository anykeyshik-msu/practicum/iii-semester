#include <stdio.h>

typedef char STYPE;
typedef unsigned char UTYPE;

#include "mz03-1.h"

int
main(void) 
{
    STYPE num, rev_num;

    scanf("%hhd", &num);

    rev_num = bit_reverse(num);

    printf("%hhd\n", rev_num);


    return 0;
}

