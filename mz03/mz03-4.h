int
satsum(int a, int b)
{
    unsigned int shifter = 0;

    shifter = ~shifter;
    int max = shifter >> 1;
    int min = shifter ^ (shifter >> 1);

    if (a > 0 && b > max - a) {
        return max;
    } else if (a < 0 && b < min - a) {
        return min;
    }
    return a + b;
}
