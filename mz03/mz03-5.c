#include <stdio.h>
#include <stdlib.h>

enum
{
    DIGIT_NUM = 10,
    BUFF_SIZE = 10000
};

char *
inputString(void);

int
main(void)
{
    char *filename;
    FILE *input;
    char *buff;
    size_t real_buff_len;
    long long counter[10] = {0};

    filename = inputString();
    buff = calloc(BUFF_SIZE, sizeof(char));
    input = fopen(filename, "r");

    if (input != NULL) {
        while (!feof(input) && !ferror(input)) {
            real_buff_len = fread(buff, sizeof(char), BUFF_SIZE, input);

            for (size_t i = 0; i < real_buff_len; i++) {
                register char tmp = buff[i];

                if (tmp >= '0' && tmp <= '9') {
                    counter[tmp - '0'] += 1;
                }
            }
        }
    }

    for (int i = 0; i < DIGIT_NUM; i++) {
        printf("%d %lld\n", i, counter[i]);
    }

    if (input != NULL) {
        fclose(input);
    }
    free(filename);

    return 0;
}

char *
inputString(void)
{
    char *str;
    int ch;
    size_t len;
    size_t size;

    len = 0;
    size = 20;

    str = realloc(NULL, sizeof(char) * size);
    if (!str) {
        return str;
    }

    while ((ch = fgetc(stdin)) != EOF && ch != '\n' && ch != '\r') {
        str[len++] = ch;

        if (len == size) {
            str = realloc(str, sizeof(char) * (size += 16));
            if (!str) {
                return str;
            }
        }
    }
    str[len++] = '\0';

    return realloc(str, sizeof(char) * len);
}
