#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

int
main(void)
{
    int fd[2];
    int pid, status;

    pipe(fd);
    pid = fork();

    if (pid > 0) {
        close(fd[1]);
        waitpid(pid, &status, 0);

        time_t curr_time;
        read(fd[0], &curr_time, sizeof(time_t));
        close(fd[0]);

        struct tm *time = malloc(sizeof(struct tm));
        localtime_r(&curr_time, time);
        printf("Y:%04d\n", time->tm_year + 1900);
        fflush(stdout);

        exit(0);
    } else {
        pid = fork();

        if (pid > 0) {
            close(fd[1]);
            waitpid(pid, &status, 0);

            time_t curr_time;
            read(fd[0], &curr_time, sizeof(time_t));
            close(fd[0]);

            struct tm *time = malloc(sizeof(struct tm));
            localtime_r(&curr_time, time);
            printf("M:%02d\n", time->tm_mon + 1);
            fflush(stdout);

            exit(0);
        } else {
            pid = fork();

            if (pid > 0) {
                close(fd[1]);
                waitpid(pid, &status, 0);

                time_t curr_time;
                read(fd[0], &curr_time, sizeof(time_t));
                close(fd[0]);

                struct tm *time = malloc(sizeof(struct tm));
                localtime_r(&curr_time, time);
                printf("D:%02d\n", time->tm_mday);
                fflush(stdout);

                exit(0);
            } else {
                time_t curr_time = time(NULL);

                write(fd[1], &curr_time, sizeof(time_t));
                write(fd[1], &curr_time, sizeof(time_t));
                write(fd[1], &curr_time, sizeof(time_t));

                close(fd[0]);
                close(fd[1]);
            }
        }
    }

    return 0;
}
