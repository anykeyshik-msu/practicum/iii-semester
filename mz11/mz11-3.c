#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    if (argc < 6) {
        exit(1);
    }

    int fd[2];
    int expr_pid, pid1, pid2, pid3, status;

    pipe(fd);
    expr_pid = fork();

    if (!expr_pid) {
        close(fd[0]);
        dup2(fd[1], 1);
        close(fd[1]);

        pid1 = fork();

        if (!pid1) {
            int fd = open(argv[4], O_RDONLY, 0);
            dup2(fd, 0);
            close(fd);

            execlp(argv[1], argv[1], NULL);
            exit(1);
        } else {
            waitpid(pid1, &status, 0);

            if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                pid2 = fork();

                if (!pid2) {
                    execlp(argv[2], argv[2], NULL);
                    exit(1);
                } else {
                    waitpid(pid2, &status, 0);
                }
            }
        }

        exit(0);
    }

    pid3 = fork();

    if (!pid3) {
        close(fd[1]);

        int out_fd =
            open(argv[5], O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);
        dup2(out_fd, 1);
        close(out_fd);

        dup2(fd[0], 0);
        close(fd[0]);

        execlp(argv[3], argv[3], NULL);
        exit(1);
    } else {
        close(fd[0]);
        close(fd[1]);

        waitpid(expr_pid, &status, 0);
        waitpid(pid3, &status, 0);
    }

    return 0;
}
