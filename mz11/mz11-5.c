#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    BASE = 10
};

int max;

void
proc(int in_fd, int out_fd, int current_num);

void
child(int *p12, int *p21, int num);

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        exit(1);
    }

    max = strtol(argv[1], NULL, BASE);

    int p12[2];
    int p21[2];

    pipe(p12);
    pipe(p21);

    if (!fork()) {
        child(p12, p21, 1);
    }
    if (!fork()) {
        child(p12, p21, 2);
    }

    int init = 1;
    FILE *finit = fdopen(p21[1], "w");

    fprintf(finit, "%d\n", init);
    fflush(stdout);

    fclose(finit);
    close(p12[0]);
    close(p12[1]);
    close(p21[0]);
    close(p21[1]);

    wait(NULL);
    wait(NULL);

    puts("Done");

    return 0;
}

void
proc(int in_fd, int out_fd, int current_num)
{
    int x;
    FILE *in = fdopen(in_fd, "r");
    FILE *out = fdopen(out_fd, "w");

    while (1) {
        if ((fscanf(in, "%d", &x)) <= 0) {
            break;
        }
        if (x >= max) {
            break;
        }

        printf("%d %d\n", current_num, x);
        fflush(stdout);

        x++;
        fprintf(out, "%d\n", x);
        fflush(out);
    }

    fclose(in);
    fclose(out);
}

void
child(int *p12, int *p21, int num)
{
    if (num == 1) {
        close(p12[0]);
        close(p21[1]);
        proc(p21[0], p12[1], 1);
        close(p12[1]);
        close(p21[0]);
    } else {
        close(p12[1]);
        close(p21[0]);
        proc(p12[0], p21[1], 2);
        close(p12[0]);
        close(p21[1]);
    }

    _exit(1);
}
