#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

void
terminate(int number, pid_t *pids);
void
pipe_close(int fd[2][2]);

int
main(int argc, char *argv[])
{
    if (argc == 1) {
        return 0;
    }

    int fd[2][2];
    if (pipe(fd[0]) == -1) {
        exit(1);
    }
    if (pipe(fd[1]) == -1) {
        exit(1);
    }

    pid_t *pids = calloc(argc - 1, sizeof(pid_t));
    for (int i = 1; i < argc; i++) {
        int pipe1 = i % 2;
        int pipe2 = (i + 1) % 2;

        close(fd[pipe2][0]);
        close(fd[pipe2][1]);

        if (pipe(fd[pipe2]) == -1) {
            pipe_close(fd);
            terminate(i - 1, pids);
        }

        pids[i] = fork();
        if (pids[i] == -1) {
            pipe_close(fd);
            terminate(i - 1, pids);
        } else {
            if (!pids[i]) {
                close(fd[pipe1][1]);
                close(fd[pipe2][0]);

                if (i != 1) {
                    if (dup2(fd[pipe1][0], 0) == -1) {
                        exit(1);
                    }
                }
                close(fd[pipe1][0]);

                if (i != argc - 1) {
                    if (dup2(fd[pipe2][1], 1) == -1) {
                        exit(1);
                    }
                }
                close(fd[pipe2][1]);

                execlp(argv[i], argv[i], NULL);
                _exit(1);
            }
        }
        close(fd[pipe1][0]);
        close(fd[pipe1][1]);

        if (pipe(fd[pipe1]) == -1) {
            pipe_close(fd);
            terminate(i - 1, pids);
        }
    }

    while (wait(NULL) > 0) {
    }

    return 0;
}

void
terminate(int number, pid_t *pids)
{
    for (int i = 0; i < number; i++) {
        if (!pids[i]) {
            break;
        }

        kill(pids[i], SIGKILL);
    }

    while (wait(NULL) > 0) {
    }

    _exit(1);
}

void
pipe_close(int fd[2][2])
{
    close(fd[0][0]);
    close(fd[0][1]);
    close(fd[1][0]);
    close(fd[1][1]);
}
