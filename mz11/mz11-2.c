#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    int fd[2];
    int pid, status;

    pipe(fd);
    pid = fork();

    if (pid > 0) {
        close(fd[0]);

        int input;
        while (scanf("%d", &input) == 1) {
            write(fd[1], &input, sizeof(input));
        }

        close(fd[1]);
        waitpid(pid, &status, 0);

        exit(0);
    } else {
        pid = fork();

        if (pid > 0) {
            close(fd[1]);
            close(fd[0]);

            waitpid(pid, &status, 0);

            exit(0);
        } else {
            close(fd[1]);

            int num;
            long long sum = 0;

            while (read(fd[0], &num, sizeof(num)) > 0) {
                sum += num;
            }
            printf("%lld\n", sum);

            close(fd[0]);
            exit(0);
        }
    }

    return 0;
}
