#include <fcntl.h>

enum
{
    OWNER_SHIFT = 6u,
    GROUP_SHIFT = 3u,
    OTHER_SHIFT = 0u
};

struct Task
{
    unsigned uid;
    int gid_count;
    unsigned *gids;            
};

static int
check_perm(mode_t mode, unsigned int type, int access)
{
    return ((mode >> type) & access) == access;
}

int
myaccess(const struct stat *stb, const struct Task *task, int access)
{
    if (task->uid == 0) {
        return 1;
    }

    if (task->uid == stb->st_uid) {
        return check_perm(stb->st_mode, OWNER_SHIFT, access);
    }

    for (int i = 0; i < task->gid_count; i++) {
        if (task->gids[i] == stb->st_gid) {
            return check_perm(stb->st_mode, GROUP_SHIFT, access);
        }
    }

    return check_perm(stb->st_mode, OTHER_SHIFT, access);
}
