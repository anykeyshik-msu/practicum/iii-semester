#include <stdio.h>
#include <stdlib.h>
#include <time.h>

enum
{
    CAL_START = 1900,
    THU_IN_WEEK = 4,
    THU_IN_NEXT_WEEK = 11,
    FIRST_DAY = 1,
    WEEK_LEN = 7
};

int
main(int argc, char const *argv[]) {
    int year;
    struct tm timeinfo = {0};

    scanf("%d", &year);
    year -= CAL_START;

    timeinfo.tm_year = year;
    timeinfo.tm_mday = FIRST_DAY;
    mktime(&timeinfo);

    if (timeinfo.tm_wday > THU_IN_WEEK) {
        timeinfo.tm_mday += THU_IN_NEXT_WEEK - timeinfo.tm_wday;
    } else {
        timeinfo.tm_mday += THU_IN_WEEK - timeinfo.tm_wday;
    }
    mktime(&timeinfo);

    int thu_num = 1;
    int curr_mon = 0;
    while (timeinfo.tm_year == year) {
        timeinfo.tm_mday += WEEK_LEN;
        mktime(&timeinfo);

        thu_num++;

        if (curr_mon != timeinfo.tm_mon) {
            thu_num = 1;
            curr_mon = timeinfo.tm_mon;
        }

        if ((thu_num % 2 == 0) && (timeinfo.tm_mday % 3 != 0)) {
            printf("%d %d\n", timeinfo.tm_mon + 1, timeinfo.tm_mday);
        }
    }

    return 0;
}

