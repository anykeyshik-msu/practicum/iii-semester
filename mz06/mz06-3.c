#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "mz06-3.h"

int
main(void)
{
    char *path1 = calloc(PATH_MAX, sizeof(char));
    char *path2 = calloc(PATH_MAX, sizeof(char));

    scanf("%s\n%s", path1, path2);

    char *tokens = relativize_path(path1, path2);

    printf("%s\n", tokens);

    free(path1);
    free(path2);

    return 0;
}
