#include <dirent.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

const char SUFFIX[] = ".exe";

enum
{
    SUFFIX_LEN = sizeof(SUFFIX) - 1
};

int
main(int argc, char **argv)
{
    if (argc < 2) {
        exit(1);
    }

    DIR *dir = opendir(argv[1]);
    if (!dir) {
        exit(1);
    }

    struct dirent *check_dir;
    char path[PATH_MAX];
    struct stat file_stat;

    unsigned long long count = 0;

    while ((check_dir = readdir(dir))) {
        int name_len = strlen(check_dir->d_name);

        if (strcmp(check_dir->d_name, ".") && strcmp(check_dir->d_name, "..")) {
            if (snprintf(
                  path, sizeof(path), "%s/%s", argv[1], check_dir->d_name) <=
                sizeof(path) - 1) {
                if (stat(path, &file_stat) == 0) {
                    if (((file_stat.st_mode & S_IFMT) == S_IFREG) &&
                        (access(path, X_OK) == 0)) {
                        if (name_len >= SUFFIX_LEN) {
                            if (!strcmp(check_dir->d_name + name_len -
                                          SUFFIX_LEN,
                                        SUFFIX)) {
                                count++;
                            }
                        }
                    }
                }
            }
        }
    }

    closedir(dir);

    printf("%lld\n", count);

    return 0;
}
