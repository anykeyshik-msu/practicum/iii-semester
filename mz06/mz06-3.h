#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

enum
{
    MAX_NEST = 1024
};

char **
normalize_path(char *path)
{
    char **token_path = calloc(MAX_NEST, sizeof(char *));

    int iter = 0;
    char *item = strtok(path, "/");
    while (item != NULL) {
        if (!strcmp(item, "..")) {
            if (iter != 0) {
                iter -= 1;
                token_path[iter] = NULL;
            }
        } else if (strcmp(item, ".") != 0) {
            token_path[iter] = item;
            iter += 1;
        }
        item = strtok(NULL, "/");
    }

    return token_path;
}

void
concat(char *s1, const char *s2)
{
    const size_t len1 = strlen(s1);
    const size_t len2 = strlen(s2);
    memcpy(s1 + len1, s2, len2 + 1);
}

char *
relativize_path(const char *path1, const char *path2)
{
    char *normalized_path = calloc(PATH_MAX, sizeof(char));
    char *wpath1 = strdup(path1);
    char **normalize_path1 = normalize_path(wpath1);
    char *wpath2 = strdup(path2);
    char **normalize_path2 = normalize_path(wpath2);

    int iter = 0;
    while (normalize_path1[iter] != NULL && 
            normalize_path2[iter] != NULL && 
            !strcmp(normalize_path1[iter], normalize_path2[iter])) {
        iter += 1;
    }

    if (normalize_path1[iter] == NULL && normalize_path2[iter] == NULL) {
        normalized_path = ".";
    } else if (normalize_path1[iter] == NULL) {
        for (int i = iter; normalize_path2[i] != NULL; i++) {
            concat(normalized_path, normalize_path2[i]);
            concat(normalized_path, "/");
        }
        normalized_path[strlen(normalized_path) - 1] = '\0';
    } else if (normalize_path2[iter] == NULL) {
        for (int i = iter; normalize_path1[i] != NULL; i++) {
            concat(normalized_path, "../");
        }
        normalized_path[strlen(normalized_path) - 1] = '\0';
    } else {
        for (int i = iter + 1; normalize_path1[i] != NULL; i++) {
            concat(normalized_path, "../");
        }
        for (int i = iter; normalize_path2[i] != NULL; i++) {
            concat(normalized_path, normalize_path2[i]);
            concat(normalized_path, "/");
        }
        normalized_path[strlen(normalized_path) - 1] = '\0';
    }

    free(normalize_path1);
    free(normalize_path2);
    free(wpath1);
    free(wpath2);

    return normalized_path;
}
