#include "mz08-2.h"
#include <stdio.h>

int
main(void)
{
    RandomGenerator *rr = random_create(1234);
    int counter;

    scanf("%d", &counter);

    for (int j = 0; j < counter; ++j) {
        printf("%d ", rr->ops->next(rr));
    }

    rr->ops->destroy(rr);

    return 0;
}
