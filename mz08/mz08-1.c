#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

enum
{
    FILE_SIZE = 65536,
    BASE = 16,
    PAGE_COUNT = 16,
    PAGE_SIZE = 512,
    FIRST_SEVEN_BITS = 65024u,
    BIT_SHIFT = 9,
    LAST_NINE_BITS = 511u
};

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        exit(1);
    }

    uint32_t start = strtoul(argv[2], NULL, BASE);

    int fd = open(argv[1], O_RDONLY);
    if (fd < 0) {
        puts("Can't read file!");
        exit(1);
    }

    struct stat st;
    fstat(fd, &st);
    if (st.st_size != FILE_SIZE) {
        exit(1);
    }

    size_t mem_size = getpagesize() * PAGE_COUNT;
    void *mapped = mmap(NULL, mem_size, PROT_READ, MAP_SHARED, fd, 0);
    if (mapped == MAP_FAILED) {
        puts("Can't map file!");
        exit(1);
    }

    uint8_t *mem_ptr = mapped;
    uint32_t virtual_addr;

    while (scanf("%x", &virtual_addr) == 1) {
        uint32_t virt_pg_name = (virtual_addr & FIRST_SEVEN_BITS) >> BIT_SHIFT;
        uint32_t phys_pg_name =
            (*((uint16_t *) &mem_ptr[start + 2 * virt_pg_name]) &
            FIRST_SEVEN_BITS) >>
            BIT_SHIFT;

        uint32_t physicl_addr = phys_pg_name * PAGE_SIZE;
        uint32_t offset = virtual_addr & LAST_NINE_BITS;
        physicl_addr += offset;

        uint32_t value = *(uint16_t *) &mem_ptr[physicl_addr];
        printf("%u\n", value);
    }

    munmap(mapped, mem_size);
    close(fd);

    return 0;
}
