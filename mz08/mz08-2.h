#include <stdio.h>
#include <stdlib.h>

enum
{
    NUMS = 100,
    START_VALUE = 1234,
    MULTIPLIER = 1103515245,
    INCREMENT = 12345,
    MODULE = 1u << 31
};

struct RandomGeneratorMethods;

typedef struct RandomGenerator
{
    unsigned int current_value;
    unsigned int multiplier;
    unsigned int increment;
    unsigned int module;
    const struct RandomGeneratorMethods *ops;
} RandomGenerator;

static int
next(RandomGenerator *random_generator)
{
    unsigned int value = random_generator->current_value;

    value *= random_generator->multiplier;
    value += random_generator->increment;
    value %= random_generator->module;

    random_generator->current_value = value;

    return random_generator->current_value;
}

static void
destroy(RandomGenerator *random_generator)
{
    free(random_generator);
}

typedef struct RandomGeneratorMethods
{
    void (*destroy)(RandomGenerator *random_generator);
    int (*next)(RandomGenerator *random_generator);
} RandomGeneratorMethods;

static const RandomGeneratorMethods random_generator_methods = {destroy, next};

RandomGenerator *
random_create(int seed)
{
    RandomGenerator *random_generator = calloc(1, sizeof(*random_generator));
    if (!random_generator) {
        exit(1);
    }

    random_generator->current_value = seed;
    random_generator->multiplier = MULTIPLIER;
    random_generator->increment = INCREMENT;
    random_generator->module = MODULE;
    random_generator->ops = &random_generator_methods;

    return random_generator;
}

