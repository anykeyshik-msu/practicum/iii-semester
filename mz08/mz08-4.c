#include <dlfcn.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum
{
    BASE = 10
};

typedef struct FuncArgs
{
    uint32_t value[16];
} FuncArgs;

int
main(int argc, char *argv[])
{
    if (argc < 3) {
        exit(1);
    }

    char *types = argv[3];

    FuncArgs func_args;
    int arg_index = 0;
    int argv_type_index = 1;
    int argv_index = 4;
    while (types[argv_type_index] != '\0') {
        if (types[argv_type_index] == 'i') {
            int tmp;

            tmp = strtol(argv[argv_index], NULL, BASE);
            func_args.value[arg_index] = tmp;

            arg_index += 1;
        } else if (types[argv_type_index] == 'd') {
            double tmp;

            tmp = strtod(argv[argv_index], NULL);
            memcpy(&func_args.value[arg_index], &tmp, sizeof(double));

            arg_index += 2;
        } else if (types[argv_type_index] == 's') {
            char *tmp;

            tmp = argv[argv_index];
            func_args.value[arg_index] = (uint32_t) tmp;

            arg_index += 1;
        }

        argv_index += 1;
        argv_type_index += 1;
    }

    void *handle;
    void *dlfunc;

    handle = dlopen(argv[1], RTLD_LAZY);
    if (!handle) {
        exit(1);
    }
    dlfunc = dlsym(handle, argv[2]);
    if (!dlfunc) {
        exit(1);
    }

    char return_type = types[0];
    if (return_type == 'v') {
        void (*func)() = (void (*)()) dlfunc;
        func(func_args);
    } else if (return_type == 'i') {
        int (*func)() = (int (*)()) dlfunc;
        int return_value = func(func_args);

        printf("%d\n", return_value);
    } else if (return_type == 'd') {
        double (*func)() = (double (*)()) dlfunc;
        double return_value = func(func_args);

        printf("%.10g\n", return_value);
    } else if (return_type == 's') {
        char *(*func)() = (char *(*) ()) dlfunc;
        char *return_value = func(func_args);

        printf("%s\n", return_value);
    } else {
        exit(1);
    }
    if (dlerror() != NULL) {
        exit(1);
    }

    dlclose(handle);

    return 0;
}
