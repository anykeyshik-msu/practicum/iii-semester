#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

typedef struct List
{
    char *dirname;
    char *dirpath;

    struct List *next;
} List;

List *
add(List *head, char *dirname, char *dirpath);

void
dirwalk(char *path);

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        exit(1);
    }

    dirwalk(argv[1]);

    return 0;
}

List *
add(List *head, char *dirname, char *dirpath)
{
    List *item = calloc(1, sizeof(List));

    item->dirname = dirname;
    item->dirpath = dirpath;

    if (!head) {
        item->next = NULL;

        return item;
    }

    List *curr_item = head;
    List *prev_item = NULL;
    while (curr_item && (strcasecmp(curr_item->dirpath, dirpath) < 0)) {
        prev_item = curr_item;
        curr_item = curr_item->next;
    }

    if (!prev_item) {
        item->next = curr_item;

        return item;
    } else {
        item->next = curr_item;
        prev_item->next = item;

        return head;
    }
}

void
dirwalk(char *path)
{
    DIR *parent_dir = opendir(path);
    if (!parent_dir) {
        return;
    }

    List *head = NULL;

    struct dirent *dir;
    while ((dir = readdir(parent_dir))) {
        if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..")) {
            if (strlen(path) + strlen(dir->d_name) <= PATH_MAX) {
                char *newpath = calloc(PATH_MAX + 1, sizeof(char));
                snprintf(newpath, PATH_MAX + 1, "%s/%s", path, dir->d_name);
                struct stat stb;

                if (lstat(newpath, &stb) == 0) {
                    if ((stb.st_mode & S_IFMT) == S_IFDIR) {
                        char *dirname = strdup(dir->d_name);
                        head = add(head, dirname, newpath);
                    } else {
                        free(newpath);
                    }
                }
            }
        }
    }
    closedir(parent_dir);

    while (head) {
        DIR *curr_dir = opendir(head->dirpath);

        if (curr_dir) {
            closedir(curr_dir);
            printf("cd %s\n", head->dirname);
            dirwalk(head->dirpath);
            puts("cd ..");
        }

        free(head->dirname);
        free(head->dirpath);

        List *tmp = head;
        head = head->next;

        free(tmp);
    }
}
