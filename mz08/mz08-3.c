#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

enum
{
    BASE = 10
};

int
main(int argc, char const *argv[])
{
    if (argc < 4) {
        exit(1);
    }

    unsigned long cols, rows;

    cols = strtol(argv[2], NULL, BASE);
    rows = strtol(argv[3], NULL, BASE);

    size_t page_size = getpagesize();
    size_t file_size;

    __builtin_umul_overflow(cols, rows, &file_size);
    __builtin_umul_overflow(file_size, sizeof(rows), &file_size);
    file_size -= 1;
    file_size /= page_size;
    file_size += 1;
    __builtin_umul_overflow(file_size, page_size, &file_size);

    int fd = open(argv[1], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (fd < 0) {
        exit(1);
    }
    if (ftruncate(fd, file_size) == -1) {
        exit(1);
    }

    void *file_ptr = mmap(NULL, file_size, PROT_WRITE, MAP_SHARED, fd, 0);
    if (file_ptr == MAP_FAILED) {
        exit(1);
    }

    unsigned long *matrix = (unsigned long *) file_ptr;

    unsigned long current_row = 0;
    unsigned long current_col = 0;
    unsigned long current_cell = 1;
    unsigned long max_cell;
    if (__builtin_umull_overflow(rows, cols, &max_cell)) {
        exit(1);
    }

    unsigned long current_i;
    while (current_cell != max_cell) {
        while (current_row < rows - 1 - current_col) {
            __builtin_umull_overflow(current_col, rows, &current_i);
            __builtin_uaddl_overflow(current_i, (current_row++), &current_i);
            matrix[current_i] = current_cell;
            current_cell += 1;
        }
        if (current_cell == max_cell) {
            break;
        }

        while (current_col < cols - (rows - current_row)) {
            __builtin_umull_overflow((current_col++), rows, &current_i);
            __builtin_uaddl_overflow(current_i, current_row, &current_i);
            matrix[current_i] = current_cell;
            current_cell += 1;
        }
        if (current_cell == max_cell) {
            break;
        }

        while (current_row > cols - 1 - current_col) {
            __builtin_umull_overflow(current_col, rows, &current_i);
            __builtin_uaddl_overflow(current_i, (current_row--), &current_i);
            matrix[current_i] = current_cell;
            current_cell += 1;
        }
        if (current_cell == max_cell) {
            break;
        }

        while (current_col > current_row + 1) {
            __builtin_umull_overflow((current_col--), rows, &current_i);
            __builtin_uaddl_overflow(current_i, current_row, &current_i);
            matrix[current_i] = current_cell;
            current_cell += 1;
        }
    }
    __builtin_umull_overflow(current_col, rows, &current_i);
    __builtin_uaddl_overflow(current_i, current_row, &current_i);
    matrix[current_i] = current_cell;

    munmap(file_ptr, file_size);

    __builtin_umul_overflow(cols, rows, &file_size);
    __builtin_umul_overflow(file_size, sizeof(rows), &file_size);
    ftruncate(fd, file_size);

    close(fd);

    return 0;
}
