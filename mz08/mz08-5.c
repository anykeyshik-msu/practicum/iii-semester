#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

enum
{
    SIZE = 16
};

void
dirwalk(const char *path);

int
str_compare(const void *s1, const void *s2);

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        exit(1);
    }

    dirwalk(argv[1]);

    return 0;
}

void
dirwalk(const char *path)
{
    DIR *parent_dir = opendir(path);
    if (!parent_dir) {
        return;
    }

    size_t size = SIZE;

    char **pathes = calloc(size, sizeof(*pathes));
    if (!pathes) {
        return;
    }

    struct dirent *dir;
    int index = 0;

    while ((dir = readdir(parent_dir))) {
        if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..")) {
            if (strlen(path) + strlen(dir->d_name) <= PATH_MAX) {
                char *newpath = calloc(PATH_MAX + 1, sizeof(char));
                if (!newpath) {
                    return;
                }

                snprintf(newpath, PATH_MAX + 1, "%s/%s", path, dir->d_name);

                struct stat stb;
                if (lstat(newpath, &stb) == 0) {
                    if ((stb.st_mode & S_IFMT) == S_IFDIR) {
                        char *dirname = strdup(dir->d_name);
                        pathes[index] = dirname;

                        index++;
                        if (index == size) {
                            size *= 2;

                            char **tmp =
                                realloc(pathes, size * sizeof(*pathes));
                            if (!tmp) {
                                return;
                            }
                            pathes = tmp;
                        }
                    } else {
                        free(newpath);
                    }
                }
            }
        }
    }
    closedir(parent_dir);

    qsort(pathes, index, sizeof(*pathes), str_compare);

    for (int i = 0; i < index; i++) {
        char curr_path[PATH_MAX];
        snprintf(curr_path, PATH_MAX, "%s/%s", path, pathes[i]);

        DIR *curr_dir = opendir(curr_path);

        if (curr_dir) {
            closedir(curr_dir);
            printf("cd %s\n", pathes[i]);

            dirwalk(curr_path);

            puts("cd ..");
        }

        free(pathes[i]);
    }

    free(pathes);
}

int
str_compare(const void *s1, const void *s2)
{
    return strcasecmp(*(const char **) s1, *(const char **) s2);
}
